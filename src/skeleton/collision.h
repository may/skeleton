int collide_aabb(Vector2 position1, Vector2 size1, Vector2 position2, Vector2 size2) {
	if (
		position1.x < position2.x + size2.x &&
		position1.x + size1.x > position2.x &&
		position1.y < position2.y + size2.y &&
		position1.y + size1.y > position2.y
	) {
		return 1;
	}
	return 0;
}
