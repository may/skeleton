#include <fxcg/display.h>

static void draw_pixel(int x, int y, unsigned int colour) {
	Bdisp_SetPoint_VRAM(x, y, colour);
}

static void clear_vram() {
	Bdisp_AllClr_VRAM();
}

static void flip_vram() {
  Bdisp_PutDisp_DD();
}

static void draw_line(int X0, int Y0, int X1, int Y1, unsigned int BaseColor, int NumLevels, unsigned int IntensityBits){
    unsigned int IntensityShift, ErrorAdj, ErrorAcc;
    unsigned int ErrorAccTemp, Weighting, WeightingComplementMask;
    int DeltaX, DeltaY, Temp, XDir;
    if (Y0 > Y1) {
        Temp = Y0; Y0 = Y1; Y1 = Temp;
        Temp = X0; X0 = X1; X1 = Temp;
    }
    draw_pixel(X0, Y0, BaseColor);

    if ((DeltaX = X1 - X0) >= 0) {
        XDir = 1;
    } else {
        XDir = -1;
        DeltaX = -DeltaX;
    }
    if ((DeltaY = Y1 - Y0) == 0) {
        /* Horizontal line */
        while (DeltaX-- != 0) {
            X0 += XDir;
            draw_pixel(X0, Y0, BaseColor);
        }
        return;
    }
    if (DeltaX == 0) {
        /* Vertical line */
        do {
            Y0++;
            draw_pixel(X0, Y0, BaseColor);
        } while (--DeltaY != 0);
        return;
    }
    if (DeltaX == DeltaY) {
        /* Diagonal line */
        do {
            X0 += XDir;
            Y0++;
            draw_pixel(X0, Y0, BaseColor);
        } while (--DeltaY != 0);
        return;
    }
    ErrorAcc = 0;
    IntensityShift = 16 - IntensityBits;
    WeightingComplementMask = NumLevels - 1;
    if (DeltaY > DeltaX) {
        ErrorAdj = ((unsigned long) DeltaX << 16) / (unsigned long) DeltaY;
        while (--DeltaY) {
            ErrorAccTemp = ErrorAcc;
            ErrorAcc += ErrorAdj;
            if (ErrorAcc <= ErrorAccTemp) {
                X0 += XDir;
            }
            Y0++;
            Weighting = ErrorAcc >> IntensityShift;
            draw_pixel(X0, Y0, BaseColor + Weighting);
            draw_pixel(X0 + XDir, Y0,
                    BaseColor + (Weighting ^ WeightingComplementMask));
        }
        draw_pixel(X1, Y1, BaseColor);
        return;
    }
    ErrorAdj = ((unsigned long) DeltaY << 16) / (unsigned long) DeltaX;
    while (--DeltaX) {
        ErrorAccTemp = ErrorAcc;
        ErrorAcc += ErrorAdj;
        if (ErrorAcc <= ErrorAccTemp) {
            Y0++;
        }
        X0 += XDir;
        Weighting = ErrorAcc >> IntensityShift;
        draw_pixel(X0, Y0, BaseColor + Weighting);
        draw_pixel(X0, Y0 + 1,
                BaseColor + (Weighting ^ WeightingComplementMask));
    }
    draw_pixel(X1, Y1, BaseColor);
}

static void draw_rectangle(Vector2 position, Vector2 size, unsigned int colour) {
	Vector2 topleft = position;
	Vector2 bottomright = add_vec2(position, size);
	draw_line(topleft.x, topleft.y, bottomright.x, topleft.y, colour, 5, 5);
	draw_line(topleft.x, bottomright.y, bottomright.x, bottomright.y, colour, 5, 5);
	draw_line(topleft.x, topleft.y, topleft.x, bottomright.y, colour, 5, 5);
	draw_line(bottomright.x, topleft.y, bottomright.x, bottomright.y, colour, 5, 5);

}

static void draw_sprite(color_t* sprite, int x, int y, int width, int height) {
   color_t* VRAM = (color_t*)GetVRAMAddress();
   VRAM += LCD_WIDTH_PX*y + x;
   for(int j=y; j<y+height; j++) {
      for(int i=x; i<x+width; i++) {
         *(VRAM++) = *(sprite++);
      }
      VRAM += LCD_WIDTH_PX-width;
   }
} 

void draw_text(int x, int y, char* text) {
	Bdisp_MMPrint(x, y, (unsigned char*)text, 2, 0xFFFFFFFF, 0, 0, 500, 0, 1, 0);
}
