#include <string.h>
#include <fxcg/keyboard.h>

const unsigned short* keyboard_register = (unsigned short*)0xA44B0000;
unsigned short lastkey[8];
unsigned short holdkey[8];

void update_keys() {
	memcpy(holdkey, lastkey, sizeof(unsigned short)*8);
	memcpy(lastkey, keyboard_register, sizeof(unsigned short)*8);
}

int key_buff(int keycode, unsigned short buff[8]) {
	int row, col, word, bit;
	row = keycode % 10;
	col = keycode/10 - 1;
	word = row >> 1;
	bit = col + 8*(row&1);
	return (0 != (buff[word] & 1<<bit));
}

int just_pressed(int keycode) {
	return key_buff(keycode, lastkey) && !key_buff(keycode, holdkey);
}

int pressed(int keycode) {
  return key_buff(keycode, holdkey);
}
