typedef struct {
	int x;
	int y;
} Vector2;

typedef struct {
	int e1;
	int e2;
	int e3;
	int e4;
} Matrix2x2;

Vector2 vec2(int x, int y) {
	Vector2 output;
	output.x = x;
	output.y = y;
	return output;
}

Matrix2x2 mat2x2(int e1, int e2, int e3, int e4) {
	Matrix2x2 output;
	output.e1 = e1;
	output.e2 = e2;
	output.e3 = e3;
	output.e4 = e4;
	return output;
}

Vector2 add_vec2(Vector2 first, Vector2 second) {
	Vector2 result;
	result.x = first.x + second.x;
	result.y = first.y + second.y;
	return result;
}
