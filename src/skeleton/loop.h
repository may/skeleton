#define SK_START_GAME_LOOP() \
	time = 0; \
	frames = 0; \
	previous_time = 0; \
	previous_frames = 0; \
	Timer_Install(5, timer_callback, 50); \
	Timer_Start(5); \
	Timer_Install(6, timer_refresh_callback, 1000); \
	Timer_Start(6); \
	Timer_Install(7, physics_update, REFRESH_RATE); \
	Timer_Start(7); \
	render(); \
	while (1) { \
		update_keys(); \
		if (just_pressed(KEY_PRGM_EXIT)) break; \
		calc_fps(); \
		render(); \
	}

int time;
int frames;

int previous_time;
int previous_frames;

int fps;

void timer_callback() {
	time += 5;
}

void timer_refresh_callback() {
	previous_time = time;
	previous_frames = frames;
	time = 0;
	frames = 0;
}

void calc_fps() {
	frames += 1;
	int tframes = frames+previous_frames;
	int ttime = time+previous_time;
	double ftime = ttime*1.0;
	double fframes = tframes*1.0;
	double ffps = 1000.0/(ftime/fframes);
	fps = (int) ffps;
}
